﻿using Microsoft.Win32.TaskScheduler;
using painel_model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace painel_model
{
    public class ScheduledTask
    {
        public string Folder { get; set; }
        public TaskState State{ get; set; }
        public string Name { get; set; }
        public string NextRunTime { get; set; }
        public string LastRunTime { get; set; }
        public string PathExecutavel { get; set; }
    }
}
