﻿using neon_sre_painel_web.SchedulerTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace neon_sre_painel_web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            SchedulerTask.SchedulerTaskClient schedulerTaskClient = new SchedulerTask.SchedulerTaskClient();
            var tasks = schedulerTaskClient.GetListTasks(new ConsoleServerTask() { });

            ViewBag.Result = tasks;


            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}