﻿using Microsoft.Win32.TaskScheduler;
using Newtonsoft.Json;
using painel_model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;

namespace neon_sre_painel_service
{
    public class SchedulerTask : ISchedulerTask
    {
        public string GetListTasks(ConsoleServerTask _consoleServerTask = null)
        {
            TaskCollection tasksCollection = null;
            var listTasks = new List<ScheduledTask>();

            try
            {
                var _ipServer = _consoleServerTask.ServerIp == null ? String.Empty : _consoleServerTask.ServerIp;

                using (TaskFolder taskFolder = new TaskService(_ipServer).GetFolder("\\"))
                {
                    var subfolders = taskFolder.SubFolders;

                    foreach (var _taskSubFolder in subfolders)
                    {
                        using (TaskFolder _subFolder = new TaskService(_ipServer).GetFolder(_taskSubFolder.Name))
                        {
                            tasksCollection = _subFolder.GetTasks();

                            foreach (var _task in tasksCollection)
                            {
                                listTasks.Add(new ScheduledTask()
                                {
                                    Folder = _task.Folder.Path,
                                    State = _task.State,
                                    LastRunTime = _task.LastRunTime.ToString("yyyy-MM-ddThh:ss"),
                                    NextRunTime = _task.NextRunTime.ToString("yyyy-MM-ddThh:ss"),
                                    Name = _task.Name,
                                    PathExecutavel = _task.Definition.Actions.ToString()
                                });
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return JsonConvert.SerializeObject(listTasks);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
