﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using neon_model.Enum;

namespace neon_model
{
    public class Retorno
    {
        public Retorno()
        {
            TipoRetorno = TipoRetorno.Erro;
            Mensagens = new List<MensagemRetorno>();
        }

        public int Codigo { get; set; }
        public TipoRetorno TipoRetorno { get; set; }
        public dynamic Conteudo { get; set; }
        public List<MensagemRetorno> Mensagens { get; set; }
    }
}
