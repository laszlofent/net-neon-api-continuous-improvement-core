﻿using neon_model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace neon_model
{
    public class MensagemRetorno
    {
        public int Codigo { get; set; }
        public string Mensagem { get; set; }
        public TipoMensagem TipoMensagem { get; set; }
    }
}
