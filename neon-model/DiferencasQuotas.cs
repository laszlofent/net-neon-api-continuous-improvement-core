﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_model
{
    public class DiferencasQuotas
    {
        public int CLIENTID { get; set; }
        public int CRKCOMPRAID { get; set; }
        public int GQ_ID_COMPRA { get; set; }
        public int GOALID { get; set; }
        public int QUOTANUMBER { get; set; }
        public int UPDATEDQUOTANUMBER { get; set; }
        public int SOLDQUOTANUMBER_VA { get; set; }
        public int SOLDQUOTANUMBER_VR { get; set; }
        public int SOLDQUOTANUMBER_VE { get; set; }
        public int SOLDQUOTANUMBER_VR_CRKERRO { get; set; }
        public decimal POSICAOBV { get; set; }
        public decimal POSICAONEONINVPOS { get; set; }
    }
}
