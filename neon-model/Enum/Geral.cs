﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_model.Enum
{
    public enum TipoRetorno
    {
        Sucesso = 0,
        Alerta = 1,
        Erro = 2
    }

    public enum TipoMensagem
    {
        Sucesso = 0,
        Erro = 1,
        ErroFatal = 2
    }
}
