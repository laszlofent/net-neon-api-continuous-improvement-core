﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_model
{
    public class RotinaConsoleProcessamento
    {
        public int Id { get; set; }
		public string Nome { get; set; }
		public string Descricao { get; set; }
		public string UltimaExecucao { get; set; }
		public bool Situacao { get; set; }
	}
}
