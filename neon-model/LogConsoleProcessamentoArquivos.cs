﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_model
{
    public class LogConsoleProcessamentoArquivos
    {
        public int Id { get; set; }
        public int RotinaConsoleProcessamentoId { get; set; }        
        public string Data_Processamento { get; set; }
        public string Final_Processamento { get; set; }
        public string Console_Nome { get; set; }
    }
}
