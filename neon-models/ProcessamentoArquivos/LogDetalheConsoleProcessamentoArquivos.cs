﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models
{
    public class LogDetalheConsoleProcessamentoArquivos
    {
        public int Id { get; set; }
        public int LogConsoleProcessamentoArquivos_Id { get; set; }
        public string DocumentoProcessado { get; set; }
        public string Mensagem { get; set; }
    }
}
