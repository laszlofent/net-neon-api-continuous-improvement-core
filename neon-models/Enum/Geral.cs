﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models.Enum
{
    public enum TipoRetorno
    {
        Sucesso = 0,
        Alerta = 1,
        Erro = 2
    }

    public enum TipoMensagem
    {
        Sucesso = 0,
        Erro = 1,
        ErroFatal = 2
    }

    public enum ErroInvestimentoCadastro
    {
        CadastroErro = 0,
        Cadastro_SemXML = 1
    }

    public enum ErroInvestimentoCompra
    {
        CompraErro = 0,
        CompraErro_SemXML = 1,
        Compra_Duplicada = 2,
        Compra_DuplicadaArredondar = 3,
        Compra_SemCard = 4,
        Compra_DeixaSaldoNegativo = 5,
    }

    public enum ErroInvestimentoVenda
    {
        VendaErro = 0,
        VendaErro_SemXML = 1,
        VendaDuplicada = 2,
        VendaDuplicadaArredondar = 3,
        VendaSemCard = 4,
        VendaDeixaSaldoNegativo = 5
    }
}
