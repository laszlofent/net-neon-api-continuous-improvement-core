﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;



namespace neon_models
{
    public class ResgateLote
    {   
        [Required(ErrorMessage ="ClientId invalido ou inexistente.")]
        public string ClientId { get; set; }

        [Required(ErrorMessage = "GoalId invalido ou inexistente.")]
        public string GoalId { get; set; }

        [Required(ErrorMessage = "Valor invalido ou inexistente.")]
        public string Valor { get; set; }

        [Required(ErrorMessage = "IgnoraBloqueioJudicial inválido ou inexistente. Favor inserir S ou N")]
        public string IgnoraBloqueioJudicial { get; set; }

        [Required(ErrorMessage = "Solicitante invalido ou inexistente."), MinLength(5, ErrorMessage = "Solicitante invalido ou inexistente.")]
        public string Solicitante { get; set; }

        public string DataSolicitacao { get; set; }

        [Required(ErrorMessage = "Solicitante invalido ou inexistente."), MinLength(5, ErrorMessage = "Solicitante invalido ou inexistente.")]
        public string Id_ServiceNow { get; set; }

    }
}
