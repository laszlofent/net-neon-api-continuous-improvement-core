﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models
{
    public class CorrecoesInvestimentoSolucao
    {
        public string PosicaoArvore { get; set; }
        public string DescricaoErro { get; set; }
        public float ValorTransacao { get; set; }
        public string ServiceNow { get; set; }
    }
}
