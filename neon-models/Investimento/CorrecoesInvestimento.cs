﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models
{
    public class CorrecoesInvestimento
    {
        public int Id { get; set; }
        public string DataProcessamento { get; set; }
        public int ClientId { get; set; }
        public int GoalQuotaId { get; set; }
        public string Solucao { get; set; }
    }
}
