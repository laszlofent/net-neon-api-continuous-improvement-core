﻿using neon_models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models
{
    public class BaseAnalitica
    {
        public int CLIENT_ID { get; set; }
        public int GOAL_ID { get; set; }
        public int GOAL_CRK_ID { get; set; }
        public int GOAL_QUOTA_ID { get; set; }
        public int GOAL_QUOTA_SALE_ID { get; set; }
        public int GOAL_QUOTA_SOLD_ID { get; set; }
        public string DATA_PROCESSAMENTO { get; set; }
        public string TIPO { get; set; }
        public int VALOR { get; set; }
        public float VALOR_CONTACORRENTE { get; set; }
        public int LANCAMENTO_DIARIO_ID { get; set; }
        public float VALOR_ESTORNO { get; set; }
        public int LANCAMENTO_MANUAL_ID { get; set; }
        public string VALOR_RETABILIDADE { get; set; }
        public int NR_OPER_LEGADO { get; set; }
        public int ERRO { get; set; }
        public int PENDENTE_ENVIO { get; set; }
        public int ENVIADO { get; set; }
        public Guid PROTOCOLO_ID { get; set; }
        public int PENDENTE_PROCESSAMENTO { get; set; }
        public int PROCESSADO_SUCESSO { get; set; }
        public int RETORNOU_XML { get; set; }
        public int PROCESSADO_ERRO { get; set; }
        public int VALOR_ERRO { get; set; }
        public string RETORNO_BV { get; set; }
        public int VALOR_TOTAL_SALDO_NEON { get; set; }
        public int VALOR_POSICAO_BV { get; set; }
        public int VALOR_INFORMADA_POSICAO_BV { get; set; }
        public float DIFF_NEON_BV { get; set; }
        public string CONSOLIDADO { get; set; }

    }
}
