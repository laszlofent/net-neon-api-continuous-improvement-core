﻿using neon_models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models
{
    public class ConsolidadoGoalQuota
    {
        public int Valor_Total_Compras_Objetivo { get; set; }
        public int Valor_Total_Compras { get; set; }
        public int Valor_Total_Compras_Erro { get; set; }
        public int Valor_Total_Compras_Pendentes { get; set; }
        public int Valor_Total_Compras_Processadas { get; set; }
        public int Valor_Total_Compras_Processadas_Erro { get; set; }
        public float Valor_Total_Compras_Debitadas_ContaCorrente { get; set; }
        public float Valor_Total_Compras_Estornadas { get; set; }
        public int Valor_Total_Vendas_Vendas { get; set; }
        public int Valor_Total_Vendas { get; set; }
        public int Valor_Total_Vendas_Erro { get; set; }
        public int Valor_Total_Vendas_Pendentes { get; set; }
        public int Valor_Total_Vendas_Processadas { get; set; }
        public int Valor_Total_Vendas_Processadas_Erro { get; set; }
        public int Valor_Total_Vendas_Creditadas_ContaCorrente { get; set; }
        public float Valor_Total_Vendas_Rentabilidade { get; set; }
        public float Valor_Total_Vendas_Estornadas { get; set; }
        public int Valor_Total_Saldo_Neon { get; set; }
        public int Valor_Posicao_Bv { get; set; }
        public int Valor_Informada_Posicao_Bv { get; set; }

    }
}
