﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models
{
    public class ControleContadores
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public float Contador { get; set; }
        public int Status { get; set; }
        public string ModificadoPor { get; set; }
    }
}
