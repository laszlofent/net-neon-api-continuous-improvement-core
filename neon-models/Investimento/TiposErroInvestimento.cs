﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models
{
    public class TiposErroInvestimento
	{
		public int Id { get; set; }
		public string Descricao { get; set; }
		public string PosicaoArvore { get; set; }
		public int Situacao { get; set; }
	}

}
