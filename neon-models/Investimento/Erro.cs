﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_models
{
    public class Erro
    {
        public string Posicao_Arvore { get; set; }
        public int Client_Id { get; set; }
        public int Goal_CRK_Compra_Id { get; set; }
        public int Goal_CRK_Venda_Id { get; set; }
        public int Goal_Quota_Venda_Id { get; set; }
        public int Goal_Quota_Compra_Id { get; set; }
        public int Goal_QuotaSale_Id { get; set; }
        public int Valor_Resgate { get; set; }
        public int Valor_Estornar { get; set; }
        public int Valor_Compra { get; set; }
        public float Vr_Pu_Posicao { get; set; }
        public int Valor_Vendido_Sucesso { get; set; }
        public int Valor_Posicao_BV { get; set; }
        public int Valor_Posicao_Neon { get; set; }
        public int Lancamento_Diario_Id { get; set; }
        public int Consolidado { get; set; }
        public DateTime Data_Processamento { get; set; }
    }

    public class ErroInvestimento
    {
        public string Script { get; set; }
        public int Id { get; set; }
        public string DataTransacao { get; set; }
        public string TipoTransacao { get; set; }
        public int ClientId { get; set; }
        public int GoalId { get; set; }
        public int GoalQuotaVendaId { get; set; }
        public int GoalQuotaSaleId { get; set; }
        public int GoalQuotaCompraId { get; set; }
        public int GoalCRKCompra { get; set; }
        public int LancamentoDiarioId { get; set; }
        public float ValorTransacao { get; set; }
        public int TipoErro { get; set; }
        public int Corrigido { get; set; }
    }

}
