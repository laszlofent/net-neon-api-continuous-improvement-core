﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using neon_application;
using neon_models;
using neon_repositorio;

namespace console_processamento_arquivos
{

    class Program
    {
        private static ProcessamentoCore _processamentoCore;
        private static CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        static void Main(string[] args)
        {
            _processamentoCore = new ProcessamentoCore(new NeonPottencialDB(), new ReportInvestimentoDB());

            try
            {

                var _rotinasHabilitadas = (List<RotinaConsoleProcessamento>)_processamentoCore.ListarRotinaConsoleProcessamento().Conteudo;

                foreach (var _rotinas in _rotinasHabilitadas.Where(s => s.Situacao == true))
                {
                    if (_rotinas.Nome == "resgate_objetivos")
                    {
                        var _idProcessamento = _processamentoCore.InserirLogProcessamento(_rotinas);

                        _processamentoCore.ResgatarObjetivosLote(((LogConsoleProcessamentoArquivos)_idProcessamento.Conteudo).Id);

                        _processamentoCore.AtualizarLogProcessamento(new LogConsoleProcessamentoArquivos()
                        {
                            Id = Convert.ToInt32(_idProcessamento),
                            Final_Processamento = DateTime.Now.ToString()
                        });
                    }

                }

            }
            catch (Exception ex)
            {
                Console.Write($"{ex.Message} \n {ex.StackTrace} \n {ex.InnerException}");
                Environment.Exit(99);
            }

            Environment.Exit(0);

            //validar diretorios

            //filtrar variaveis

            //carregar arquivo

            //Realizar validação

            //Processar movimento

            //Gerar log

            //Finalizar
        }
    }
}
