﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_core
{
    public class BaseClass
    {

        public static void MoveArquivoErro(string _arquivoProcessado, StringBuilder _mensagem)
        {
            FileInfo _file = new FileInfo(_arquivoProcessado);

            if (!Directory.Exists($"{_file.Directory}/ERRO")){
                Directory.CreateDirectory($"{_file.Directory}/ERRO");
            }

            File.Move(_file.FullName, $"{_file.Directory}/ERRO/{_file.Name}");

            using (StreamWriter w = File.AppendText($"{_file.Directory}/ERRO/{_file.Name}_erro"))
            {
                w.WriteLine(_mensagem.ToString());
            }
        }

        public static void MoveArquivoProcessado(string _arquivoProcessado, StringBuilder _mensagem)
        {
            FileInfo _file = new FileInfo(_arquivoProcessado);

            if (!Directory.Exists($"{_file.Directory}/PROCESSADO")){
                Directory.CreateDirectory($"{_file.Directory}/PROCESSADO");
            }

            File.Move(_file.FullName, $"{_file.Directory}/PROCESSADO/{_file.Name}");

            using (StreamWriter w = File.AppendText($"{_file.Directory}/PROCESSADO/{_file.Name}_processado"))
            {
                w.WriteLine(_mensagem.ToString());
            }
        }
    }
}
