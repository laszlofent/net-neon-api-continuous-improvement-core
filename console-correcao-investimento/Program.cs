﻿using neon_models;
using neon_models.Enum;
using neon_application;
using neon_repositorio;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;

namespace console_correcao_investimento
{
    class Program
    {
        private static CorrecaoBaseCore _correcaoBaseCore;
        private static CorrecaoCadastroCore _correcaoCadastroCore;
        private static CorrecaoCompraCore _correcaoCompraCore;
        private static CorrecaoVendaCore _correcaoVendaCore;

        private static CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private static List<Task> _processosCorrecao;

        static void Main(string[] args)
        {
            _correcaoBaseCore = new CorrecaoBaseCore(new InvestimentoRepositorio());
            _processosCorrecao = new List<Task>();

            try
            {

                _processosCorrecao.Add(Task.Run(() => CorrecaoCompraCore.ComprasLimboN1()));
                

                Task.WaitAll(_processosCorrecao.ToArray());                


            }
            catch (Exception ex)
            {
                Console.Write($"{ex.Message} \n {ex.StackTrace} \n {ex.InnerException}");
                Environment.Exit(99);
            }

            Environment.Exit(0);
        }
    }
}
