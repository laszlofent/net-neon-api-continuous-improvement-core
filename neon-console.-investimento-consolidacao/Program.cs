﻿using neon_core;
using neon_models;
using neon_repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_console_investimento_consolidacao
{
    class Program
    {
        private static ConsolidacaoCore _consolidacaoCore;        

        public Program()    
        {
            
        }

        static void Main(string[] args)
        {
            _consolidacaoCore = new ConsolidacaoCore();

            Console.Write("Iniciando processamento do Consolidador Diário");

            _consolidacaoCore.PersistirDiferencasQuotas();

            Console.Write("Fim do processamento do Consolidador Diário");            
            
        }
    }
}

