﻿using Dapper;
using neon_models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_repositorio
{
    public class InvestimentoRepositorio
    {
        private static ReportInvestimentoDB _reportDb;

        public InvestimentoRepositorio()
        {

        }

        #region ESTRUTURA
        public bool CarregarBaseAnalitica()
        {
            var retorno = false;

#if(DEBUG)
            var _query = $"EXEC ops.spc_CarregarBaseAnalitica 2";
#endif

#if(RELEASE)
            var _query = $"EXEC ops.spc_CarregarBaseAnalitica";
#endif

            retorno = ExecutarQueryReportInvestimento<bool>(_query).FirstOrDefault();

            return retorno;
        }

        public List<TiposErroInvestimento> ListarTiposErroInvestimento() {
        
            var retorno = new List<TiposErroInvestimento>();
            var _query = $"EXEC ops.spc_ListarTiposErroInvestimento";

            retorno = ExecutarQueryReportInvestimento<TiposErroInvestimento>(_query).ToList();

            return retorno;
        }

        public void IdentificarComoCorrigido(ErroInvestimento _erroInvestimento)
        {
            var retorno = false;

            var _query = $"UPDATE [ops].[ErrosInvestimentos] SET " +
                $"Corrigido = 1 " +
                $"WHERE Id = {_erroInvestimento.Id}";

            retorno = ExecutarQueryReportInvestimento<bool>(_query).FirstOrDefault();
        }

        public IEnumerable<T> ExecutarQueryReportInvestimento<T>(string _query)
        {
            var retorno = default(IEnumerable<T>);

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportInvestimento"].ToString()))
            {
                try
                {
                    con.Open();

                    retorno = con.Query<T>(sql: _query, commandType: CommandType.Text, commandTimeout: 3600);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }

            return retorno;
        }

        #endregion


        #region CADASTRO

        #endregion


        #region COMPRA
        public List<Erro> ListarComprasLimboN1()
        {
            var retorno = new List<Erro>();

            var _query = $"EXEC ops.spc_ListarComprasLimbo_N1";

            retorno = ExecutarQueryReportInvestimento<Erro>(_query).ToList();

            return retorno;
        }

        #endregion


        #region VENDA

        public int CarregarVendasSemXML_N1()
        {
            var _query = $"EXEC ops.spc_CarregarVendasSemXML_N1";

            return ExecutarQueryReportInvestimento<int>(_query).FirstOrDefault();
        }

        public IEnumerable<ErroInvestimento> ListarVendasSemXML_N1(string _dataReferencia = null)
        {
            var _query = $"EXEC ops.spc_ListarVendasSemXML_N1 0";

            return ExecutarQueryReportInvestimento<ErroInvestimento>(_query).ToList();

        }

        #endregion
    }
}
