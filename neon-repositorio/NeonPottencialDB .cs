﻿using Neon.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;


namespace neon_repositorio
{
    public class NeonPottencialDB : IDisposable
    {
        private IDbConnection _conexao;
        bool conexaoGerenciavel = false;
        private bool isDisposed;

        public NeonPottencialDB()
        {
            try
            {
                _conexao = CriarConexao();
            }
            catch(Exception ex)
            {
                throw;
            }
        }


        public IDbConnection Conexao { get { return _conexao; } set { _conexao = value; conexaoGerenciavel = true; } }

        ~NeonPottencialDB()
        {
            Dispose();
        }
        public void Dispose()
        {
            try
            {
                if (!isDisposed)
                {
                    if (_conexao != null)
                    {
                        _conexao.Dispose();
                        if (_conexao.State != ConnectionState.Closed)
                            _conexao.Close();
                        _conexao = null;
                    }

                    isDisposed = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static IDbConnection CriarConexao()
        {
            var retorno = new SqlConnection(NeonConfig.ConnNeonDb);

            return retorno;
        }

        public T Executar<T>(Func<IDbConnection, T> func)
        {
            var retorno = default(T);

            if (func == null)
                return retorno;

            if (_conexao == null)
                _conexao = CriarConexao();

            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            retorno = func(_conexao);

            return retorno;
        }
    }

}