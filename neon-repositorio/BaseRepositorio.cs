﻿using Dapper;
using neon_models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_repositorio
{
    public class BaseRepositorio
    {
        private static ReportInvestimentoDB _reportDb;

        public BaseRepositorio()
        {
            
        }

        #region Console_Correcao_Investimento

        public bool AtualizarConsolidadoQuotasNeon(ControleContadores _controleContadores)
        {
            _reportDb = new ReportInvestimentoDB();

            var retorno = false;

            var _query = $"EXEC ops.spc_AtualizarConsolidadoGoalQuotas_Neon " +
                $"{_controleContadores.Nome}, " +
                $"{_controleContadores.Contador} ";

            retorno = ExecutarQuery<bool>(_query).FirstOrDefault();

            return retorno;
        }

        public List<Erro> ListarResgatesLimbo()
        {
            _reportDb = new ReportInvestimentoDB();

            var retorno = new List<Erro>();

            var _query = $"EXEC ops.spc_ListarResgatesLimbo";

            retorno = ExecutarQuery<Erro>(_query).ToList();

            return retorno;
        }


        public List<Erro> ListarResgatesSemXML()
        {
            _reportDb = new ReportInvestimentoDB();

            var retorno = new List<Erro>();

            var _query = $"EXEC ops.spc_ListarResgatesSemXML";

            retorno = ExecutarQuery<Erro>(_query).ToList();

            return retorno;
        }

        public List<QueryExecucao> ObterScriptCorrigirXML(Erro _resgateErro)
        {
            _reportDb = new ReportInvestimentoDB();

            var retorno = new List<QueryExecucao>();

            var _query = $"EXEC ops.spc_CorrigirXMLSBSCRK " +
                $"{_resgateErro.Goal_CRK_Compra_Id}," +
                $"{_resgateErro.Goal_CRK_Venda_Id}," +
                $"{_resgateErro.Valor_Resgate}," +
                $"{_resgateErro.Client_Id}," +
                $"{_resgateErro.Goal_Quota_Compra_Id}," +
                $"{_resgateErro.Goal_Quota_Venda_Id}," +
                $"{_resgateErro.Goal_QuotaSale_Id}," +
                $"0";

            retorno = ExecutarQuery<QueryExecucao>(_query).ToList();

            return retorno;
        }

        public List<BaseAnalitica> ObterBaseAnalitica()
        {
            _reportDb = new ReportInvestimentoDB();

            var retorno = new List<BaseAnalitica>();

            var _query = $"SELECT * FROM OPS.GoalQuotas_Neon (NOLOCK)";

            retorno = ExecutarQuery<BaseAnalitica>(_query).ToList();

            return retorno;
        }

        private IEnumerable<T> ExecutarQuery<T>(string _query)
        {
            var retorno = default(IEnumerable<T>);

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportInvestimento"].ToString()))
            {
                try
                {
                    con.Open();

                    retorno = con.Query<T>(sql: _query, commandType: CommandType.Text, commandTimeout: 3600);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }

            return retorno;
        }

        #endregion
    }
}
