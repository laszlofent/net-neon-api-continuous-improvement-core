﻿using neon_repositorio;
using neon_models;
using neon_models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading;
using System.Configuration;
using CsvHelper;
using Newtonsoft.Json;
using Model.Neon;
using Business.Neon;

namespace neon_application
{
    public class CorrecaoVendaCore : BaseClass
    {
        InvestimentoRepositorio _correcaoRepositorio;

        public CorrecaoVendaCore(InvestimentoRepositorio correcaoRepositorio)
        {
            _correcaoRepositorio = correcaoRepositorio;
            _TiposErrosInvestimento = _correcaoRepositorio.ListarTiposErroInvestimento();
        }
        
        public void VendasSemXML_N1()
        {
            try
            {
                var _querys = new StringBuilder();

                var retornoProcessamentoDiff = _correcaoRepositorio.CarregarVendasSemXML_N1();

                var listaVendasSemXML_N1 = _correcaoRepositorio.ListarVendasSemXML_N1();

                foreach(var vendaSemXML_N1 in listaVendasSemXML_N1)
                {
                    
                }

                //Executar querys

                //Percorrer todas as querys e executar na NEONPottencial

                //Atualizar registro de erro como corrigodo

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}

