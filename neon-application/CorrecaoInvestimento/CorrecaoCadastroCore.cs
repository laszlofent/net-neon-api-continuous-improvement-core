﻿using neon_repositorio;
using neon_models;
using neon_models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading;
using System.Configuration;
using CsvHelper;
using Newtonsoft.Json;
using Model.Neon;
using Business.Neon;

namespace neon_application
{
    public class CorrecaoCadastroCore : BaseClass
    {
        InvestimentoRepositorio _correcaoRepositorio;

        public CorrecaoCadastroCore(InvestimentoRepositorio correcaoRepositorio)
        {
            _correcaoRepositorio = correcaoRepositorio;
            _TiposErrosInvestimento = _correcaoRepositorio.ListarTiposErroInvestimento();
        }

    }
}

