﻿using neon_repositorio;
using neon_models;
using neon_models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading;
using System.Configuration;
using CsvHelper;
using Newtonsoft.Json;

namespace neon_application
{
    public class ProcessamentoCore : BaseClass
    {
        ProcessamentoRepositorio _processamentoArquivosRepositorio;

        public ProcessamentoCore(NeonPottencialDB neonDb, ReportInvestimentoDB reportDb)
        {
            _processamentoArquivosRepositorio = new ProcessamentoRepositorio(neonDb, reportDb);
        }


        public Retorno ListarRotinaConsoleProcessamento()
        {
            var retorno = new Retorno();

            try
            {

                retorno.Conteudo = _processamentoArquivosRepositorio.ListarRotinasProcessamento();

                if (retorno.Conteudo != null)
                {
                    retorno.TipoRetorno = TipoRetorno.Sucesso;
                }

            }
            catch (Exception ex)
            {
                retorno.Mensagens = InserirMensagemErro(retorno.Mensagens, ex);
            }

            return retorno;
        }

        public Retorno InserirLogProcessamento(RotinaConsoleProcessamento _ConsoleProcessamentoArquivo)
        {
            var retorno = new Retorno();

            try
            {
                if (_ConsoleProcessamentoArquivo == null)
                {
                    var _mensagem = $"{new StackFrame(0).GetMethod().Name} => Nome da console não informado.";
                    retorno.Mensagens.Add(new MensagemRetorno() { Codigo = 10, Mensagem = _mensagem, TipoMensagem = TipoMensagem.Erro });

                }

                if (ExisteMensagemErro(retorno.Mensagens))
                    return retorno;

                retorno.Conteudo = _processamentoArquivosRepositorio.InserirLogProcessamento(_ConsoleProcessamentoArquivo);

                if (retorno.Conteudo != null)
                {
                    retorno.TipoRetorno = TipoRetorno.Sucesso;
                }

            }
            catch (Exception ex)
            {
                retorno.Mensagens = InserirMensagemErro(retorno.Mensagens, ex);
            }

            return retorno;
        }

        public Retorno AtualizarLogProcessamento(LogConsoleProcessamentoArquivos _logConsoleProcessamentoArquivos)
        {
            var retorno = new Retorno();

            try
            {
                if (_logConsoleProcessamentoArquivos == null)
                {
                    var _mensagem = $"{new StackFrame(0).GetMethod().Name} => Objeto _logConsoleProcessamentoArquivos não informado";
                    retorno.Mensagens.Add(new MensagemRetorno() { Codigo = 10, Mensagem = _mensagem, TipoMensagem = TipoMensagem.Erro });
                }


                if (ExisteMensagemErro(retorno.Mensagens))
                    return retorno;

                retorno.Conteudo = _processamentoArquivosRepositorio.AtualizarLogProcessamento(_logConsoleProcessamentoArquivos);

                if (retorno.Conteudo != null)
                {
                    retorno.TipoRetorno = TipoRetorno.Sucesso;
                }

            }
            catch (Exception ex)
            {
                retorno.Mensagens = InserirMensagemErro(retorno.Mensagens, ex);
            }

            return retorno;
        }

        public Retorno InserirDetalheLogProcessamento(LogDetalheConsoleProcessamentoArquivos _logDetalheConsoleProcessamentoArquivos)
        {
            var retorno = new Retorno();

            try
            {
                if (_logDetalheConsoleProcessamentoArquivos == null)
                {
                    var _mensagem = $"{new StackFrame(0).GetMethod().Name} => Objeto de detalhe de processamento não informado.";
                    retorno.Mensagens.Add(new MensagemRetorno() { Codigo = 10, Mensagem = _mensagem, TipoMensagem = TipoMensagem.Erro });
                }
                else if (_logDetalheConsoleProcessamentoArquivos.DocumentoProcessado == null)
                {
                    var _mensagem = $"{new StackFrame(0).GetMethod().Name} => Documento processado não informado.";
                    retorno.Mensagens.Add(new MensagemRetorno() { Codigo = 10, Mensagem = _mensagem, TipoMensagem = TipoMensagem.Erro });
                }
                else
                {

                }

                if (ExisteMensagemErro(retorno.Mensagens))
                    return retorno;

                retorno.Conteudo = _processamentoArquivosRepositorio.InserirDetalheLogProcessamento(_logDetalheConsoleProcessamentoArquivos);

                if (retorno.Conteudo != null)
                {
                    retorno.TipoRetorno = TipoRetorno.Sucesso;
                }

            }
            catch (Exception ex)
            {
                retorno.Mensagens = InserirMensagemErro(retorno.Mensagens, ex);
            }

            return retorno;
        }

        public Retorno ResgatarObjetivosLote(int _LogConsoleProcessamentoArquivos_Id)
        {
            var retorno = new Retorno();
            string _filePath;

            try
            {
                var _pathRaiz = ConfigurationManager.AppSettings["diretorio_raiz_solic_resgate"].ToString();

                var _arquivosPendentes = Directory.GetFiles(_pathRaiz, "*.csv");

                foreach (var _arquivo in _arquivosPendentes)
                {
                    var _sbProcessamento = new StringBuilder();
                    var _arquivoLido = new List<ResgateLote>();
                    var _mensagem = String.Empty;

                    FileInfo _arquivoInfo = new FileInfo(_arquivo);

                    #region LEITURA

                    using (var reader = new StreamReader(_arquivoInfo.FullName))
                    using (var csv = new CsvReader(reader))
                    {
                        _arquivoLido = csv.GetRecords<ResgateLote>().ToList();
                    }

                    #endregion

                    #region VALIDACAO


                    var _errosLinha = new List<ValidationResult>();

                    foreach (var _linha in _arquivoLido)
                    {
                        _errosLinha = ValidarSolicitacaoResgate(_linha).ToList();
                        foreach (var error in _errosLinha)
                        {
                            _sbProcessamento.AppendLine($"ClientId: {_linha.ClientId} \t GoalId: {_linha.GoalId} \t {error.ErrorMessage}");
                        }
                    }

                    #endregion

                    #region RESGATE


                    if (_errosLinha.Count() == 0)
                    {
                        //Percorrer a lista e realizar a baixa do saldo quota
                    }
                    else
                        MoveArquivoErro(_arquivoInfo.FullName, _sbProcessamento);

                    #endregion

                    
                    InserirDetalheLogProcessamento(new LogDetalheConsoleProcessamentoArquivos()
                    {
                        DocumentoProcessado = JsonConvert.SerializeObject(_arquivoLido),
                        LogConsoleProcessamentoArquivos_Id = _LogConsoleProcessamentoArquivos_Id,
                        Mensagem = _mensagem

                    });

                }

            }
            catch (Exception ex)
            {
                retorno.Mensagens.Add(new MensagemRetorno() { Codigo = 99, Mensagem = ex.Message, TipoMensagem = TipoMensagem.Erro });
                retorno.Mensagens.Add(new MensagemRetorno() { Codigo = 99, Mensagem = ex.StackTrace, TipoMensagem = TipoMensagem.Erro });
            }


            return retorno;
        }

        private List<MensagemRetorno> InserirMensagemErro(List<MensagemRetorno> _mensagens, Exception ex)
        {
            _mensagens.Add(new MensagemRetorno() { Codigo = ex.HResult, Mensagem = ex.Message, TipoMensagem = TipoMensagem.ErroFatal });
            _mensagens.Add(new MensagemRetorno() { Codigo = ex.HResult, Mensagem = ex.StackTrace, TipoMensagem = TipoMensagem.ErroFatal });
            _mensagens.Add(new MensagemRetorno() { Codigo = ex.HResult, Mensagem = ex.InnerException.ToString(), TipoMensagem = TipoMensagem.ErroFatal });

            return _mensagens;
        }

        private bool ExisteMensagemErro(List<MensagemRetorno> _mensagens)
        {
            var retorno = true;

            var _mensagensErro = _mensagens.Where(m => m.TipoMensagem == TipoMensagem.Erro).Count();

            if (_mensagensErro == 0)
                retorno = false;

            return retorno;
        }

        private IEnumerable<ValidationResult> ValidarSolicitacaoResgate(object _arquivo)
        {
            var resultadoValidacao = new List<ValidationResult>();
            var contexto = new ValidationContext(_arquivo, null, null);
            Validator.TryValidateObject(_arquivo, contexto, resultadoValidacao, true);
            return resultadoValidacao;
        }
    }
}
