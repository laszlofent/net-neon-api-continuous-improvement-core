﻿using Dapper;
using neon_models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neon_repository
{
    public class ProcessamentoRepositorio
    {
        private static NeonPottencialDB _neonDb;
        private static ReportInvestimentoDB _reportDb;

        public ProcessamentoRepositorio(NeonPottencialDB neonDB, ReportInvestimentoDB reportDB)
        {
            _neonDb = neonDB;
            _reportDb = reportDB;
        }

        #region Console_ProcessamentoArquivos

        public LogConsoleProcessamentoArquivos InserirLogProcessamento(string _nomeConsoleProcessamentoArquivo)
        {
            var retorno = new LogConsoleProcessamentoArquivos();

            var _query = $"INSERT INTO ops.LogConsoleProcessamentoArquivos VALUES (" +
                $"GETDATE(), " +
                $"NULL,'" +
                $"{_nomeConsoleProcessamentoArquivo}')" +
                $"\t" +
                $"SELECT * FROM ops.LogConsoleProcessamentoArquivos WHERE Id = @@IDENTITY";

            retorno = _reportDb.Executar(cn => cn.Query<LogConsoleProcessamentoArquivos>(sql: _query, commandType: CommandType.Text)).FirstOrDefault();

            return retorno;
        }

        public int AtualizarLogProcessamento(LogConsoleProcessamentoArquivos _logProcessamentoArquivo)
        {
            var retorno = 0;

            var _query = $"UPDATE ops.LogConsoleProcessamentoArquivos SET " +
                $"Tempo_Processamento = {_logProcessamentoArquivo.Final_Processamento} " +
                $"WHERE Id = {_logProcessamentoArquivo.Id}" +
                $"\n " +
                $"SELECT @@IDENTITY";

            retorno = _reportDb.Executar(cn => cn.Query<int>(sql: _query, commandType: CommandType.Text)).FirstOrDefault();

            return retorno;
        }

        public int InserirDetalheLogProcessamento(LogDetalheConsoleProcessamentoArquivos _logDetalheConsoleProcessamentoArquivos)
        {
            var retorno = 0;

            var _query = $"INSERT INTO ops.LogConsoleProcessamentoArquivos VALUES (" +
                $"{_logDetalheConsoleProcessamentoArquivos.LogConsoleProcessamentoArquivos_Id}, " +
                $"{_logDetalheConsoleProcessamentoArquivos.DocumentoProcessado}, " +
                $"{_logDetalheConsoleProcessamentoArquivos.Mensagem}) " +
                $"\n " +
                $"SELECT  @@IDENTITY";

            retorno = _reportDb.Executar(cn => cn.Query<int>(sql: _query, commandType: CommandType.Text)).FirstOrDefault();

            return retorno;
        }

        public List<RotinaConsoleProcessamento> ListarRotinasProcessamento()
        {
            var retorno = new List<RotinaConsoleProcessamento>();

            var _query = $"SELECT * FROM ops.RotinaConsoleProcessamento";

            retorno = _reportDb.Executar(cn => cn.Query<RotinaConsoleProcessamento>(sql: _query, commandType: CommandType.Text)).ToList();

            return retorno;
        }

        public int AtualizarRotinasProcessamento(RotinaConsoleProcessamento _rotinaConsoleProcessamento)
        {
            var retorno = 0;

            var _query = $"UPDATE ops.RotinaConsoleProcessamento SET " +
                $"UltimaExecucao = {_rotinaConsoleProcessamento.UltimaExecucao} " +
                $"WHERE Id = {_rotinaConsoleProcessamento.Id}" +
                $"\n " +
                $"SELECT @@IDENTITY";

            retorno = _reportDb.Executar(cn => cn.Query<int>(sql: _query, commandType: CommandType.Text)).FirstOrDefault();

            return retorno;
        }

        #endregion
    }
}
